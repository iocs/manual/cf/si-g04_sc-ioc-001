#
# Module: essioc
#
require essioc
require modbus

#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")


#
# IOC: SI-G04:SC-IOC-001
# Load .iocsh file
#
iocshLoad("$(E3_CMD_TOP)/iocsh/si_g04.iocsh", "BMCNAME=ConVen-G04:HVAC-BMC-01, IPADDR=172.17.15.11, RECVTIMEOUT=3000")
