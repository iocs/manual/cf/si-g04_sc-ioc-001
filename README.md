# SI-G04:SC-IOC-001

Forced ventillation interface with Siemens AG for G04

Converted from [m-epics-bmc_conven_g04_hvac_bmc_01](https://bitbucket.org/europeanspallationsource/m-epics-bmc_conven_g04_hvac_bmc_01)

## Responsible

Miklós Boros ([email](mailto://miklos.boros@ess.eu))
